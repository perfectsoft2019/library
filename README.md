# AnyHedge Library

Library for creation, validation and management of AnyHedge contracts.


## Installation

Install the library via NPM:

```
# npm install @generalprotocols/anyhedge
```


## Example

Developing with AnyHedge is easy but depends on some base knowledge of developing with Bitcoin Cash.
The usage instructions below show the key steps and have links to further documentation.
We also include a [custodial funding example](./examples/custodial.js) as a reference that shows both the preparation and the AnyHedge steps.


## Usage


### Setting up

Include the library into your project:

```js
// Load the AnyHedge library.
const anyHedgeLibrary = require('@generalprotocols/anyhedge');

// Create an instance of the contract manager.
const anyHedgeManager = new anyHedgeLibrary();
```


### Creating a contract

[Create](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#create) a new contract and [build](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#build) it to get access to the contract address:

```js
// Create new contract.
let contractData = await anyHedgeManager.create(...parameters);

// Build a contract instance.
let contractInstance = await anyHedgeManager.build(contractData.hashes);

// Derive the contract address.
let contractAddress = contractInstance.getAddress('mainnet');
```


### Registering a contract for automatic redemption

[Register](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#register) a contract for automatic redemption:

```js
// Submit a contract for external redemption management.
let contractData = await anyHedgeManager.register(...parameters);
```

After registering a contract, [validate](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#validate) that the returned contract is correct:

```js
// Validate a contract address.
let contractValidity = await anyHedgeManager.validate(...parameters);
```


### Funding a contract

**[WIP]** Fund a contract:

```js
// STEPS:
// 1. Determine who the parties to the contract will be.
// 2. Determine what unspent coins the parties will use.
// 3. Craft a transaction with following inputs covered:
//    - contractData.metadata.hedgeInputSats
//    - contractData.metadata.longInputSats
//    - contractData.metadata.dustCost
//    - contractData.metadata.minerCost
// 4. Pass the transaction to each party for signing.
// 5. Broadcast the funding transaction.
```


### Manually redeeming a contract

If you need to, you can [mature](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#mature) or [liquidate](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#liquidate) a contract manually:

*Note that this is not necessary if you are using a redemption service as above.*

```js
// Liquidate a contract.
let liquidationResult = await anyHedgeManager.liquidate(...parameters);

// Mature a contract.
let maturationResult = await anyHedgeManager.mature(...parameters);
```


### Mutual redemption of a contract

Also if needed, you can mutually redeem a contract by agreement between Hedge and Long in several ways:

- [abort](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#abort) a contract to return original inputs to their owners.
- [cancel](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#cancel) a contract to pay out the contract with custom output values.
- [settle](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#settle) a contract to pay out as if it was matured at a given price.

```js
// Cancel a contract to pay back initial funding.
let abortResult = await contract.abort(...parameters);

// Cancel a contract to pay back initial funding.
let cancelResult = await contract.cancel(...parameters);

// Cancel a contract to pay out current valuation.
let settlementResult = await contract.settle(...parameters);
```
