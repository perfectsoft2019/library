import { Usecase } from './interfaces';

const usecase: Usecase =
{
	/*
	The data in this usecase example represents a 1-week long hedging contract of $10.
	The initial data was derived from a functional example with the following on-chain activity:
	NOTE: The data was updated manually at different times because of changes to the library and contract
	*/

	contract:
	{
		register:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				1000,
				23600,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output:
			{
				contract:
				{
					id: 1,
					address: 'bitcoincash:pq07sf9fwuyhs6tyrv2fymm88qhzeaqulvhgrmn7fd',
				},
				parameters:
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.alloc(0),
					highLowDeltaTruncatedZeroes: Buffer.alloc(1),
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				metadata:
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					startBlockHeight: 615643,
					maturityModifier: 1009,
					earliestLiquidationModifier: 0,
					highLiquidationPriceMultiplier: 10,
					lowLiquidationPriceMultiplier: 0.75,
					startPrice: 23600,
					hedgeUnits: 1000,
					totalInputSats: 5649718,
					hedgeInputSats: 4237288,
					longInputSats: 1412430,
					dustCost: 1092,
					minerCost: 1500,
					longInputUnits: 333.33348,
				},
				hashes:
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			},
		},
		create:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				1000,
				23600,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output:
			{
				parameters:
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.alloc(0),
					highLowDeltaTruncatedZeroes: Buffer.alloc(1),
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				metadata:
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					startBlockHeight: 615643,
					maturityModifier: 1009,
					earliestLiquidationModifier: 0,
					highLiquidationPriceMultiplier: 10,
					lowLiquidationPriceMultiplier: 0.75,
					startPrice: 23600,
					hedgeUnits: 1000,
					totalInputSats: 5649718,
					hedgeInputSats: 4237288,
					longInputSats: 1412430,
					dustCost: 1092,
					minerCost: 1500,
					longInputUnits: 333.33348,
				},
				hashes:
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('c663854280ca1e7d21f273ec498ca09b5686df56', 'hex'),
				},
			},
		},
		validate:
		{
			input:
			[
				'bitcoincash:pq07sf9fwuyhs6tyrv2fymm88qhzeaqulvhgrmn7fd',
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				1000,
				23600,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output: true,
		},
	},
	liquidation:
	{
		simulate:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.alloc(0),
					highLowDeltaTruncatedZeroes: Buffer.alloc(1),
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				5652310,
				17500,
			],
			output:
			{
				hedgePayoutSats: 5650231,
				longPayoutSats: 547,
				payoutSats: 5650778,
				minerFeeSats: 1532,
			},
		},
	},
	maturation:
	{
		simulate:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.alloc(0),
					highLowDeltaTruncatedZeroes: Buffer.alloc(1),
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				5652310,
				23500,
			],
			output:
			{
				hedgePayoutSats: 4255351,
				longPayoutSats: 1394431,
				payoutSats: 5649782,
				minerFeeSats: 2528,
			},
		},
	},
};

export = usecase;
