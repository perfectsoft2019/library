export interface ContractData
{
	contract?: ContractRegistration;
	parameters: ContractParameters;
	metadata: ContractMetadata;
	hashes: ContractHashes;
}

export interface ContractRegistration
{
	id: number;
	address: string;
}

export interface ContractParameters
{
	lowLiquidationPrice: number;
	highLiquidationPrice: number;
	earliestLiquidationHeight: number;
	maturityHeight: number;
	oraclePubk: Buffer;
	hedgeLockScript: Buffer;
	longLockScript: Buffer;
	hedgeMutualRedeemPubk: Buffer;
	longMutualRedeemPubk: Buffer;
	lowTruncatedZeroes: Buffer;
	highLowDeltaTruncatedZeroes: Buffer;
	hedgeUnitsXSatsPerBchHighTrunc: number;
	payoutSatsLowTrunc: number;
}

export interface ContractMetadata
{
	oraclePublicKey: string;
	hedgePublicKey: string;
	longPublicKey: string;
	startBlockHeight: number;
	maturityModifier: number;
	earliestLiquidationModifier: number;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
	startPrice: number;
	hedgeUnits: number;
	longInputUnits: number;
	totalInputSats: number;
	hedgeInputSats: number;
	longInputSats: number;
	dustCost: number;
	minerCost: number;
}

export interface ContractHashes
{
	mutualRedemptionDataHash: Buffer;
	payoutDataHash: Buffer;
}

export interface SimulationOutput
{
	hedgePayoutSats: number;
	longPayoutSats: number;
	payoutSats: number;
	minerFeeSats: number;
}
