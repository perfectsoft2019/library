import { debug, range } from './javascript-util';
import { SCRIPT_INT_MAX, MAX_TRUNC_BYTES, DUST_LIMIT } from '../constants';
import { ContractMetadata } from '../interfaces';
import Long from 'long';
import { bigIntToScriptNumber } from '@bitauth/libauth';

/**
* Helper function to bitshift down with truncation.
*
* @param data    a number to bitshift and truncate.
* @param bytes   how many bytes to bitshift.
*
* @returns the bitshifted and then truncated number.
*/
export const truncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'trunc() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data / (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'trunc() =>', result ]);

	// Return the variable buffer encoded data.
	return result;
};

/**
* Helper function to bitshift up with truncation.
*
* @param data    a number to bitshift and truncate.
* @param bytes   how many bytes to bitshift.
*
* @returns the bitshifted and then truncated number.
*/
export const untruncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'untrunc() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data * (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'untrunc() =>', result ]);

	// Return the variable buffer encoded data.
	return result;
};

/**
* Helper function to determine how many bytes we need to shift to fit a number
* within the bitcoin cash script limitations.
*
* @param value   a number to determine bitshift requirements for.
*
* @returns an integer stating how many bytes we need to shift the value.
*/
export const calculateRequiredScriptNumTruncation = function(value: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTruncationSize() <=', arguments ]);

	let truncationLevel;
	for(const truncationSize in range(MAX_TRUNC_BYTES + 1))
	{
		// Truncate the value at this size.
		const truncatedValue = truncScriptNum(value, Number(truncationSize));

		// Check if this truncation size is sufficient..
		if(truncatedValue >= 1 && truncatedValue <= SCRIPT_INT_MAX)
		{
			// Store the current size as a suitable truncatin level.
			truncationLevel = Number(truncationSize);

			// Stop looking now that we have a suitable truncation level.
			break;
		}
	}

	if(truncationLevel === undefined)
	{
		// Since no truncation size was matched, throw an error.
		throw(new Error(`Failed to find a suitable truncation level for '${value}'`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTruncationSize() =>', truncationLevel ]);

	// return the matching truncation size.
	return truncationLevel;
};

/**
 * Helper function to calculate the size of a script num
 *
 * @param scriptNum   a number to check the script num size.
 *
 * @returns the byte size of the passed number when encoded as a script num.
 */
export const scriptNumSize = function(scriptNum: number): number
{
	return bigIntToScriptNumber(BigInt(scriptNum)).byteLength;
};

/**
* Helper function to calculate the total sats for a contract (inputs, miner fees, dust cost)
*
* @param contractMetadata   a ContractMetadata object including the sats information for a contract.
*
* @returns the number of total sats that are contained in the contract.
*/
export const calculateTotalSats = function(contractMetadata: ContractMetadata): number
{
	const { hedgeInputSats, longInputSats, minerCost, dustCost } = contractMetadata;
	const totalSats = hedgeInputSats + longInputSats + minerCost + dustCost;

	return totalSats;
};

/**
* Helper function to make an output amount of sats dust safe by performing a bitwise OR
* with the DUST value (546)
*
* @param satoshis   a number of satoshis to make dust safe.
*
* @returns the dust safe version of the number of satoshis.
*/
export const dustsafe = function(satoshis: number): number
{
	return Long.fromNumber(satoshis)
		.or(DUST_LIMIT)
		.toNumber();
};
